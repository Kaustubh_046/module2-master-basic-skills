## Object Detection ## 
Object Detection is a common Computer Vision problem which deals with identifying and locating object of certain classes in the image. Interpreting the object localisation can be done in various ways, including creating a bounding box around the object or marking every pixel in the image which contains the object.  
![](https://miro.medium.com/max/875/1*j_zE5G5zttpWLd5hXsv0jA.jpeg)  
 ### Common model architectures used for object detection: ###

* R-CNN
* Fast R-CNN
* Faster R-CNN
* Mask R-CNN
* SSD (Single Shot MultiBox Defender)
* YOLO (You Only Look Once)
* Objects as Points
* Data Augmentation Strategies for Object Detection

### Applications of Object detection ###

Object detection is applied in numerous territories of image processing, including picture retrieval, security, observation, computerized vehicle systems and machine investigation. 
