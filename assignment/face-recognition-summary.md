## Face Recognition ##
Facial recognition is a system built to identify a person from an image or video. To be more precise, recognition software identifies unique facial and body features and translates them into a unique ID, which is then used to link the unique features to a user and or the user’s information.

### Working ###
Facial analysis capabilities allow users to understand where faces exist in an image or video, as well as the attributes of those faces. For example, the software analyzes attributes such as how far apart your eyes are, what colour they are, what your mood is, what colour your hair is and what the visual geometry of your face is.


![](https://image.slidesharecdn.com/aipresentation1-151118131003-lva1-app6892/95/face-recognition-8-638.jpg?cb=1447852323)  
### Applications ###


![](https://www.nec.com/en/global/techrep/journal/g16/n01/images/160108_02.jpg)
