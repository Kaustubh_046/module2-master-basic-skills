## Segmentation ##
![](https://i2.wp.com/neptune.ai/wp-content/uploads/semantic_vs_instance.png?ssl=1)  
Image segmentation takes it to a new level by trying to find out accurately the exact boundary of the objects in the image.  

#### Image Segmentation ####
We know an image is nothing but a collection of pixels. Image segmentation is the process of classifying each pixel in an image belonging to a certain class and hence can be thought of as a classification problem per pixel. There are two types of segmentation techniques.  
1. **Semantic segmentation:** Semantic segmentation is the process of classifying each pixel belonging to a particular label.
2. **Instance segmentation:** Instance segmentation differs from semantic segmentation in the sense that it gives a unique label to every instance of a particular object in the image.

#### Use Cases: ####

1. **Google portrait mode:** There are many use-cases where it is absolutely essential to separate foreground from background.
2. **Virtual try-on:** Virtual try on of clothes is an interesting feature which was available in stores using specialized hardware which creates a 3d model.
3. **Self-driving cars:** Self driving cars need a complete understanding of their surroundings to a pixel perfect level. Hence image segmentation is used to identify lanes and other necessary information.
